const {group, slugify} = require('./util')
const _ = require('lodash')
const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

const util = {
    stringify: JSON.stringify,
    group,
    slugify
}

const promoCategoriesTemplate = 
    _.template(fs.readFileSync(path.join(__dirname, './templates/promoCategories.yml')))
const promosByCategoryTemplate = 
    _.template(fs.readFileSync(path.join(__dirname, './templates/promosByCategory.yml')))

exports.generatePromoCategoriesFlow = function generatePromoCategoryFlow(categories) {
    const resultYaml = promoCategoriesTemplate({
        categories,
        util
    })
    return {
        promo: yaml.safeLoad(resultYaml)
    }
}

exports.generatePromoByCategoryFlow = function (category, promos) {
    const resultYaml = promosByCategoryTemplate({
        category, promos, util
    })
    const flow = yaml.safeLoad(resultYaml)
    return ['promo_category_' + slugify(category.name), flow]
}

function createPromoKeywordArray (categoryName) {
    // Remove symbols
    return categoryName.replace(/[^a-zA-Z ]/g, '')
        .split(' ').map(w => w.toLowerCase())
}

exports.generatePromoCategoriesNLU = function (categories) {
    const promoCategoriesKeyword = categories
        .map(c => [slugify(c.name), [...createPromoKeywordArray(c.name)]])
        .reduce((agg, p) => Object.assign(agg, {[p[0]]: p[1]}), {})
    return {
        promo: {
            type: 'keyword',
            options: {
                keywords: promoCategoriesKeyword
            }
        }
    }
}