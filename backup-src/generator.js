const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const {createFlow: createMenuFlow} = require('./menu.js')
const { createFlow: createProductFlow, createNlu: createProductNlu } = require('./product')
const { generatePromoCategoriesFlow, generatePromoByCategoryFlow,  generatePromoCategoriesNLU} = require('./promo')
const { slugify, parseProduct } = require('./util')
const createApi = require('./api')

const flowsDir = './bot/flows'

let productNLU, promoCategoriesNLU, Nlu

async function generate(config) {
    const api = createApi(config)
    let [ products, version, promoCategories ] = await Promise.all([
        api.getProducts().then(res => res.data.data),
        api.getCurrentBotVersion(),
        api.getPromoCategories().then(res => res.data.data)
    ])

    let bot = {
        schema: 'kata.ai/schema/kata-ml/1.0',
        name: config.botName,
        desc: 'Sabrina BRI Bot Helper',
        version: '0.0.0',
        config: {
            botDisplayName: config.botDisplayName,
            bankName: config.bankName,
            bankPhoneNumber: config.bankPhoneNumber,
            apiBaseUrl: config.apiBaseUrl,
            defaultCarouselImage: config.defaultCarouselImage,
            defaultCarouselUrl: config.defaultCarouselUrl,
            maxRecursion: 10,
        },
        id: config.botId,
        flows: {},
        nlus: {},
        methods: {
            "parseTime(message, context, data, options)": {
                "code": "function parseTime(message, context, data, options){let date = new Date(context.$now + 300000); let dateISO = date.toISOString(); let parsedDate = dateISO.split('T'); let parsedTime = parsedDate[1].split('.'); data.parsedTime = parsedDate[0] + ' ' + parsedTime[0]; let dateEnd = new Date(context.$now + 360000); let dateISOEnd = dateEnd.toISOString(); let parsedDateEnd = dateISOEnd.split('T'); let parsedTimeEnd = parsedDateEnd[1].split('.'); data.parsedTimeEnd = parsedDateEnd[0] + ' ' + parsedTimeEnd[0];}",
                "entry": "parseTime"
            },
            "productNLU(message, options, config)": {
                code: `
                function slugify(text) {
                    return text.toString().toLowerCase()
                        .replace(/\\s+/g, '_')
                        .replace(/[^\\w\\-]+/g, '')
                        .replace(/\\_\\_+/g, '_')
                        .replace(/^_+/, '')
                        .replace(/_+$/, '');
                }
                function match (keywords, message) {
                    return keywords.map(k => message.toLowerCase().indexOf(k.toLowerCase()) !== -1)
                        .find(m => m === true) !== undefined;
                }
                function scan (products, message, weight) {
                    if (!products || products.length <= 0) return [];
                    let matches = [];
                    for (let product of products) {
                        if (match(product.keyword, message)) {
                            matches.push([slugify(product.title), weight]);
                        }
                        matches = matches.concat(scan(product.subProducts, message, weight + 1));
                    }
                    return matches;
                }
                function productNLU (msg, opo, opoiki, opomeneh, config) {
                    const products = config.productHierarchy;
                    const matchesProduct = scan(products, msg.content, 0);
                    if (matchesProduct.length > 0) {
                        return matchesProduct.reduce((a, b) => a[1] < b[1] ? b : a)[0];
                    }
                    return [];
                }
                `,
                entry: 'productNLU'
            }
        }
    }

    bot.version = version

    Nlu = parseNlus()
    productNLU = createProductNlu(products)
    promoCategoriesNLU = generatePromoCategoriesNLU(promoCategories)
    bot.nlus = Object.assign({},
        Nlu,
        exceptionNLU(),
        productNLU,
        promoCategoriesNLU
    )
    
    // Its bit of a hack
    products.push(mainProduct = {
        id: 0,
        title: 'Main',
        keyword: ['product', 'produk']
    })
    const productFlows = generateProductFlow(products)

    // Main Menu
    const mainMenu = generateMenuFlow(products)
    
    // Promo
    const promoCategoriesFlow = generatePromoCategoriesFlow(promoCategories)
    const promoByCategoryFlow = await generatePromoByCategoryFlows(promoCategories)
    bot.flows = Object.assign({}, mainMenu, parseFlow(), promoCategoriesFlow, promoByCategoryFlow, productFlows)
    // Configs
    // Products
    const productsConfig = generateProductsConfig(products)
    bot.config.products = productsConfig
    
    // Promos
    const promosConfig = await generatePromosConfig(promoCategories, api)
    bot.config.promos = promosConfig
    bot.config.promoCategories = promoCategories
    bot.config.productHierarchy = parseProduct(products)
    // Push
    const res = await api.pushBot(bot)
    console.log('Pushing bot to version ' + res.data.version)
    // console.log('Successfull update to ' + res.data.version)
    return res.data
}

// Parse flow from yaml to js
function parseFlow () {
    return fs.readdirSync(path.join(__dirname, flowsDir)).map(filename => {
        const flowName = filename.substring(0, filename.indexOf('.yml'))
        const flow = yaml.safeLoad(fs.readFileSync(path.join(__dirname, flowsDir, filename)))
        return [flowName, flow]
    }).reduce((agg, flow) => Object.assign(agg, {[flow[0]]: flow[1]}), {})
}

function parseNlus() {
    return yaml.safeLoad(fs.readFileSync(path.join(__dirname, './bot/nlu.yml')))
}

function exceptionNLU() {
    let exceptionKeyword = Object.assign({},
        Nlu.menu.options.keywords,
        Nlu.back.options.keywords,
        productNLU.product.options.keywords,
        promoCategoriesNLU.promo.options.keywords
    )
    return {
        exception: {
            type: 'keyword',
            options: {
                keywords: exceptionKeyword
            }
        }
    }
}

function generateMenuFlow(products) {    
    return createMenuFlow(products.filter(p => p.parent_id === 0))
}

function generateProductFlow(products) {
    return products.map((product, index, products) => 
        createProductFlow(product, products.filter(p => p.parent_id === product.id))
    ).reduce((agg, flow) => Object.assign(agg, {[flow[0]]: flow[1]}), {})
}

function generateProductNlu(products) {
    return createProductNlu(products)
}

async function generatePromoByCategoryFlows(categories) {
    return categories.map(category => generatePromoByCategoryFlow(category))
        .reduce((agg, flow) => Object.assign(agg, {[flow[0]]: flow[1]}), {})
}

function generateProductsConfig(products) {
    return products.map((product, index, products) => {
        const subProducts = products.filter(p => p.parent_id === product.id)
            .map(sub => Object.assign({}, sub, {
                hasSub: products.find(p => p.parent_id === sub.id) !== undefined
            }))
        return [slugify(product.title), subProducts]
    }
    ).reduce((agg, [productTitle, subProducts]) => Object.assign(agg, {[productTitle]: subProducts}), {})
}

async function generatePromosConfig(promoCategories, api) {
    var pendingPromos = promoCategories.map(category => api.getPromoByCategoryId(category.id).then(res => res.data.data).then(promos => [slugify(category.name), promos]))
    return (await Promise.all(pendingPromos))
        .reduce((agg, [categoryName, promos]) => Object.assign(agg, {[categoryName]: promos}), {})
}

module.exports = generate