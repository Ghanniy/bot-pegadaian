const axios = require('axios')

// Promise FTW
module.exports = config => ({
    // KATAAI API
    deployBot: (deploymentId, version) => axios.put(config.kataaiBaseUrl + `/bots/${config.botId}/deployments/${deploymentId}`, {
        botVersion: version
    }, {
        headers: {
            Authorization: `Bearer ${config.kataaiToken}`
        }
    }).catch(err => console.error('Failed deploy bot: ', err)),
    pushBot: (bot) => axios.put(`${config.kataaiBaseUrl}/bots/${config.botId}?increment=patch`, bot, {
        headers: {
            Authorization: `Bearer ${config.kataaiToken}`
        }
    }).catch(err => console.error('Failed push bot: ', err)),
    getCurrentBotVersion: () => axios.get(`${config.kataaiBaseUrl}/bots/${config.botId}`, {
        headers: {
            Authorization: `Bearer ${config.kataaiToken}`
        }
    }).then(res => res.data.version).catch(err => console.error('Failed get bot version: ', err)),
    // SKYSHI API
    getProducts: () => axios.get(`${config.apiBaseUrl}/product?page_size=100`)
        .catch(err => console.error('Failed getting products: ', err)),
    getPromoCategories: () => axios.get(`${config.apiBaseUrl}/promo-event/category?page_size=100`)
        .catch(err => console.error('Failed getting promo categories: ', err)),
    getPromoByCategoryId: (id) => axios.get(`${config.apiBaseUrl}/promo-event?category_id=${id}&page_size=100`)
        .catch(err => console.error('Failed getting promos: ', err)),
    getLastUpdate: () => axios.get(`${config.apiBaseUrl}/lastmodified`).then(res => res.data.data.lastmodified)
        .catch(err => console.error('Failed getting lastmodified: ', err)),
})