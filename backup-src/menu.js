const {group, slugify} = require('./util')
const _ = require('lodash')
const fs = require('fs')
const yaml = require('js-yaml')
const path = require('path')

const util = {
    stringify: JSON.stringify,
    group,
    slugify
}

const menuTemplate = _.template(fs.readFileSync(path.join(__dirname, './templates/menu.yml')))

function createFlow (products) {
    const resultYaml = menuTemplate({
        products,
        util
    })
    return yaml.safeLoad(resultYaml)
}

exports.createFlow = function (products) {
    const flow = createFlow(products)
    return {"menu": flow}
}