// Check api every 5 minutes, deploy new bot if change detected
// Simple
const generate = require('./generator')
const createApi = require('./api')

class Checker {
    constructor (api) {
        this.lastUpdate = null
        this.api = api
    }
    async shouldUpdate() {
        const contentLastUpdate = await this.api.getLastUpdate()
        if (!this.lastUpdate) {
            this.lastUpdate = Math.floor(Date.now() / 1000)
            return true
        }
        if (!contentLastUpdate) {
            return false
        }
        if (Number(contentLastUpdate) > Number(this.lastUpdate)) {
            this.lastUpdate = Number(contentLastUpdate)
            return true
        }
        return false
    }
}

// Main loop
async function mainLoop(config, interval, deploments, checker, api) {
    if (!api) {
        api = createApi(config)
    }
    if (!checker) {
        checker = new Checker(api)
    }
    let update = false
    try {
        update = await checker.shouldUpdate()
    } catch (err) {
        console.error('Error checking for update: ', err)
        process.exit(1)
    }
    if (!update) {
        console.log('No need to update')
    }
    else {
        console.log('Update bot')
        try {
            const bot = await generate(config)
            await Promise.all(
                deploments
                    .map(id => api.deployBot(id, bot.version)
                        .then(res => console.log(`Success deploy to ${id} version ${res.data.botVersion}`))))
                .then(() => console.log('Successfully deploy bot'))
                .catch(err => console.error('Error deploying bot: ', err))
        } catch (err) {
            console.log('Error deploying bot: ', err)
            process.exit(1)
        }
    }
    console.log(`Waiting for ${interval} ms`)
    setTimeout(() => mainLoop(config, interval, deploments, checker, api), interval)
}

module.exports = mainLoop